package com.tharinduhasthika.attendancetracker.libs

import android.view.View
import androidx.recyclerview.widget.RecyclerView

abstract class AppViewHolder<T>(itemView: View, private val listener: OnItemClickListener<T>) :
    RecyclerView.ViewHolder(itemView),
    View.OnClickListener,
    View.OnLongClickListener
{

    protected var item: T? = null

    interface OnItemClickListener<T> {
        fun onItemClick(item: T)
        fun onItemLongClick(item: T)
    }

    override fun onClick(v: View?) {
        listener.onItemClick(item!!)
    }

    override fun onLongClick(v: View?): Boolean {
        listener.onItemLongClick(item!!)
        return true
    }



}