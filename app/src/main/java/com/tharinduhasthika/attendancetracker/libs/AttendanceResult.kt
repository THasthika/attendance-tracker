package com.tharinduhasthika.attendancetracker.libs

import com.tharinduhasthika.attendancetracker.db.AbsentEntry
import com.tharinduhasthika.attendancetracker.db.AbsentEntryType
import com.tharinduhasthika.attendancetracker.db.Course

class AttendanceResult(
    val course: Course,
    val totalPercentage: Float,
    val theoryPercentage: Float,
    val practicalPercentage: Float
) {
    companion object {
        fun calculateResult(course: Course, absentEntries: List<AbsentEntry>): AttendanceResult {

            var tah = 0
            var pah = 0
            for (absentEntry in absentEntries) {
                if (absentEntry.medicalSubmitted) continue
                if (absentEntry.type === AbsentEntryType.THEORY) {
                    tah += absentEntry.hours
                } else {
                    pah += absentEntry.hours
                }
            }

            val cth = course.theoryHours
            val cph = course.practicalHours

            if (cth == 0)
                tah = 0
            if (cph == 0)
                pah = 0

            val tp = if (cth == 0) -1f else (1 - tah.toFloat() / cth) * 100f
            val pp = if (cph == 0) -1f else (1 - pah.toFloat() / cph) * 100f
            val tt = (1 - (tah + pah).toFloat() / (cth + cph)) * 100f

            return AttendanceResult(course, tt, tp, pp)
        }
    }
}
