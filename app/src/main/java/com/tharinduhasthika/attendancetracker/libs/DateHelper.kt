package com.tharinduhasthika.attendancetracker.libs

import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*


object DateHelper {

    fun dateToString(date: Date): String {
        val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
        return dateFormat.format(date)
    }

    fun stringToDate(string: String): Date? {
        val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
        try {
            return dateFormat.parse(string)
        } catch (e: ParseException) {
            return null
        }

    }

    fun dateToDisplayDate(date: Date): String {
        val dateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        return dateFormat.format(date)
    }

}