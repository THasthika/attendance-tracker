package com.tharinduhasthika.attendancetracker.libs

import android.content.Context
import com.tharinduhasthika.attendancetracker.R
import com.tharinduhasthika.attendancetracker.db.AbsentEntryType

object StringHelper {

    fun getAbsentEntryTypeString(absentEntryType: AbsentEntryType, context: Context) : String {
        return when (absentEntryType) {
            AbsentEntryType.THEORY -> context.getString(R.string.theory)
            AbsentEntryType.PRACTICAL -> context.getString(R.string.practical)
        }
    }

}