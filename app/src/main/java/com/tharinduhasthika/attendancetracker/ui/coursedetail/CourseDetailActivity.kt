package com.tharinduhasthika.attendancetracker.ui.coursedetail

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.tharinduhasthika.attendancetracker.R
import com.tharinduhasthika.attendancetracker.db.AbsentEntry
import com.tharinduhasthika.attendancetracker.db.Course
import com.tharinduhasthika.attendancetracker.libs.AppViewHolder
import com.tharinduhasthika.attendancetracker.libs.AttendanceResult
import com.tharinduhasthika.attendancetracker.ui.absententryedit.AbsentEntryEditActivity
import com.tharinduhasthika.attendancetracker.ui.courseedit.CourseEditActivity

import kotlinx.android.synthetic.main.activity_course_detail.*
import kotlinx.android.synthetic.main.content_course_detail.view.*
import java.util.*

class CourseDetailActivity : AppCompatActivity(), AppViewHolder.OnItemClickListener<AbsentEntry> {

    companion object {
        const val DATA_COURSE_ID = "CourseDetailActivity_COURSE_ID"

        const val DATA_COURSE_EDIT_CODE = 0x101
        const val DATA_COURSE_EDIT_CODE_OK = 0x100
    }

    private lateinit var viewModel: CourseDetailViewModel

    private var mAbsentEntries: List<AbsentEntry>? = null
    private var mCourse: Course? = null
    private var mAttendanceResult: AttendanceResult? = null

    private val mCourseDetailAdapter = CourseDetailAdapter(this, this)

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_course_detail, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        val id = item.itemId

        when (id) {
            R.id.menu_edit -> if (mCourse != null) {
                val intent = Intent(this, CourseEditActivity::class.java)
                intent.putExtra(CourseEditActivity.DATA_COURSE_ID, mCourse!!.id)
                startActivityForResult(intent, DATA_COURSE_EDIT_CODE)
            }
            R.id.menu_delete -> if (mCourse != null) {
                AlertDialog.Builder(this)
                    .setTitle(R.string.delete_course_title)
                    .setMessage(R.string.delete_course_message)
                    .setIcon(R.drawable.ic_action_alert)
                    .setPositiveButton(android.R.string.yes) { _, _ ->
                        viewModel.delete(mCourse!!)
                        onBackPressed()
                    }
                    .setNegativeButton(android.R.string.no, null).show()
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_course_detail)
        setSupportActionBar(toolbar)

        supportActionBar!!.elevation = 0f

        val courseId = intent.getIntExtra(DATA_COURSE_ID, -1)

        viewModel = ViewModelProviders.of(this).get(CourseDetailViewModel::class.java)
        viewModel.setCourseID(courseId)

        viewModel.getCourse().observe(this, Observer {
            mCourse = it
            updateCourse()
        })

        viewModel.getAbsentEntries().observe(this, Observer {
            mAbsentEntries = it
            updateCourse()
        })

        content.absent_entries_recyclerview.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.HORIZONTAL))
        content.absent_entries_recyclerview.layoutManager = LinearLayoutManager(this)
        content.absent_entries_recyclerview.setHasFixedSize(false)
        content.absent_entries_recyclerview.adapter = mCourseDetailAdapter

        fab.setOnClickListener {
            if (mCourse != null) {
                val intent = Intent(this@CourseDetailActivity, AbsentEntryEditActivity::class.java)
                intent.putExtra(AbsentEntryEditActivity.COURSE_ID, mCourse!!.id)
                startActivity(intent)
            }
        }
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            DATA_COURSE_EDIT_CODE -> if (resultCode == DATA_COURSE_EDIT_CODE_OK) {
                onBackPressed()
            }
        }

        super.onActivityResult(requestCode, resultCode, data)
    }

    private fun updateCourse() {
        if (mCourse == null || mAbsentEntries == null) return

        mAttendanceResult = AttendanceResult.calculateResult(mCourse!!, mAbsentEntries!!)
        supportActionBar!!.title = mCourse!!.name

        content.tv_percentage.text = String.format(Locale.getDefault(), "%.2f%%", mAttendanceResult!!.totalPercentage)
        if (mAttendanceResult!!.practicalPercentage == -1f) {
            content.tv_practical_percentage_label.visibility = View.GONE
            content.tv_practical_percentage.visibility = View.GONE
        } else {
            content.tv_practical_percentage_label.visibility = View.VISIBLE
            content.tv_practical_percentage.visibility = View.VISIBLE
            content.tv_practical_percentage.text = String.format(
                Locale.getDefault(), "%.2f%%", mAttendanceResult!!.practicalPercentage)
        }
        if (mAttendanceResult!!.theoryPercentage == -1f) {
            content.tv_theory_percentage_label.visibility = View.GONE
            content.tv_theory_percentage.visibility = View.GONE
        } else {
            content.tv_theory_percentage_label.visibility = View.VISIBLE
            content.tv_theory_percentage.visibility = View.VISIBLE
            content.tv_theory_percentage.text = String.format(
                Locale.getDefault(), "%.2f%%", mAttendanceResult!!.theoryPercentage)
        }

        mCourseDetailAdapter.absentEntries = mAbsentEntries!!
    }

    override fun onItemClick(item: AbsentEntry) {
        if (mCourse != null) {
            val intent = Intent(this@CourseDetailActivity, AbsentEntryEditActivity::class.java)
            intent.putExtra(AbsentEntryEditActivity.COURSE_ID, mCourse!!.id)
            intent.putExtra(AbsentEntryEditActivity.ABSENT_ENTRY_ID, item.id)
            startActivity(intent)
        }
    }

    override fun onItemLongClick(item: AbsentEntry) {
        if (mCourse != null) {
            AlertDialog.Builder(this)
                .setTitle("Delete Absent Entry")
                .setMessage("Really want to delete the absent entry?")
                .setIcon(R.drawable.ic_action_alert)
                .setPositiveButton(
                    android.R.string.yes
                ) { _, _ -> viewModel.delete(item) }
                .setNegativeButton(android.R.string.no, null).show()
        }
    }

}
