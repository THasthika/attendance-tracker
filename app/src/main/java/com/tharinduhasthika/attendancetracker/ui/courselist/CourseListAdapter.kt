package com.tharinduhasthika.attendancetracker.ui.courselist

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tharinduhasthika.attendancetracker.R
import com.tharinduhasthika.attendancetracker.db.Course
import com.tharinduhasthika.attendancetracker.libs.AppViewHolder
import com.tharinduhasthika.attendancetracker.libs.AttendanceResult
import kotlinx.android.synthetic.main.item_course_list.view.*
import java.util.*

class CourseListAdapter(private val listener: AppViewHolder.OnItemClickListener<Course>) : RecyclerView.Adapter<CourseListAdapter.ViewHolder>() {

    var attendanceResults: List<AttendanceResult> = arrayListOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_course_list, parent, false)
        return ViewHolder(view, listener)
    }

    override fun getItemCount(): Int {
        return attendanceResults.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.setAttendanceResult(attendanceResults[position])
    }


    class ViewHolder(itemView: View, listener: OnItemClickListener<Course>) : AppViewHolder<Course>(itemView, listener) {

        init {
            itemView.setOnClickListener(this)
            itemView.setOnLongClickListener(this)
        }

        fun setAttendanceResult(attendanceResult: AttendanceResult) {
            item = attendanceResult.course
            itemView.tv_name.text = attendanceResult.course.name
            itemView.tv_percentage.text = String.format(Locale.getDefault(), "%.2f%%", attendanceResult.totalPercentage)
            if (attendanceResult.practicalPercentage == -1f) {
                itemView.tv_practical_percentage_label.visibility = View.GONE
                itemView.tv_practical_percentage.visibility = View.GONE
            } else {
                itemView.tv_practical_percentage_label.visibility = View.VISIBLE
                itemView.tv_practical_percentage.visibility = View.VISIBLE
                itemView.tv_practical_percentage.text = String.format(
                    Locale.getDefault(), "%.2f%%", attendanceResult.practicalPercentage)
            }
            if (attendanceResult.theoryPercentage == -1f) {
                itemView.tv_theory_percentage_label.visibility = View.GONE
                itemView.tv_theory_percentage.visibility = View.GONE
            } else {
                itemView.tv_theory_percentage_label.visibility = View.VISIBLE
                itemView.tv_theory_percentage.visibility = View.VISIBLE
                itemView.tv_theory_percentage.text = String.format(
                    Locale.getDefault(), "%.2f%%", attendanceResult.theoryPercentage)
            }
        }
    }

}