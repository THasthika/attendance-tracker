package com.tharinduhasthika.attendancetracker.ui.splash

import android.content.Intent
import android.os.Bundle
import android.util.Log
import com.google.android.material.snackbar.Snackbar
import androidx.appcompat.app.AppCompatActivity;
import com.tharinduhasthika.attendancetracker.R
import com.tharinduhasthika.attendancetracker.ui.courselist.CourseListActivity

import kotlinx.android.synthetic.main.activity_splash.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.util.*

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        GlobalScope.launch {
            delay(1000)
            val intent = Intent(this@SplashActivity, CourseListActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

}
