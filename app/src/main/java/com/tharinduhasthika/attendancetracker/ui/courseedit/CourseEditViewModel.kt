package com.tharinduhasthika.attendancetracker.ui.courseedit

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.tharinduhasthika.attendancetracker.db.AppRepository
import com.tharinduhasthika.attendancetracker.db.Course
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class CourseEditViewModel(application: Application) : AndroidViewModel(application) {

    private var parentJob = Job()
    private val coroutineContext: CoroutineContext
        get() = parentJob + Dispatchers.Main
    private val scope = CoroutineScope(coroutineContext)

    private val repository = AppRepository(application)

    fun getCourseByID(id: Int) : LiveData<Course> {
        return repository.getCourseByID(id)
    }

    fun insert(course: Course) = scope.launch(Dispatchers.IO) {
        repository.insert(course)
    }

    fun update(course: Course) = scope.launch(Dispatchers.IO) {
        repository.update(course)
    }

    fun delete(course: Course) = scope.launch(Dispatchers.IO) {
        repository.delete(course)
    }

    override fun onCleared() {
        super.onCleared()
        parentJob.cancel()
    }

}