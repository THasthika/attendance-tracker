package com.tharinduhasthika.attendancetracker.ui.courseedit

import android.app.AlertDialog
import android.content.DialogInterface
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.Menu
import android.view.MenuItem
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.tharinduhasthika.attendancetracker.R
import com.tharinduhasthika.attendancetracker.db.Course
import com.tharinduhasthika.attendancetracker.ui.coursedetail.CourseDetailActivity
import kotlinx.android.synthetic.main.activity_course_edit.*
import kotlinx.android.synthetic.main.content_course_edit.*
import kotlinx.android.synthetic.main.content_course_edit.view.*
import java.util.*

class CourseEditActivity : AppCompatActivity() {

    private var mCourse : Course? = null
    private lateinit var viewModel: CourseEditViewModel

    companion object {
        const val DATA_COURSE_ID = "CourseEditActivity_COURSE_ID"
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_course_edit, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onPrepareOptionsMenu(menu: Menu): Boolean {
        if (mCourse == null || mCourse!!.id == 0) {
            menu.getItem(0).isVisible = false
        }
        return super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        when (id) {
            R.id.menu_delete -> if (mCourse != null && mCourse!!.id != 0) {
                AlertDialog.Builder(this)
                    .setTitle(R.string.delete_course_title)
                    .setMessage(R.string.delete_course_message)
                    .setIcon(R.drawable.ic_action_alert)
                    .setPositiveButton(android.R.string.yes) { _, _ ->
                        viewModel.delete(mCourse!!)
                        onCourseDelete()
                    }
                    .setNegativeButton(android.R.string.no, null).show()
            }
        }

        return super.onOptionsItemSelected(item)
    }

    private fun onCourseDelete() {
        setResult(CourseDetailActivity.DATA_COURSE_EDIT_CODE_OK)
        finish()
    }

    override fun onNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_course_edit)

        setSupportActionBar(toolbar)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setDisplayShowHomeEnabled(true)

        viewModel = ViewModelProviders.of(this).get(CourseEditViewModel::class.java)

        setUpTextChangeListeners()

        val courseID = intent.getIntExtra(DATA_COURSE_ID, 0)
        if (courseID == 0) {
            supportActionBar!!.setTitle(R.string.new_course)
            mCourse = Course(0, "", 0, 0)
        } else {
            viewModel.getCourseByID(courseID).observe(this, Observer {
                mCourse = it
                updateCourseDetails()
            })
        }
    }

    private fun updateCourseDetails() {
        if (mCourse == null) {
            supportActionBar!!.setTitle(R.string.new_course)
        } else {
            supportActionBar!!.title = getString(R.string.edit) + " " + mCourse!!.name
            content.input_course_name.editText!!.setText(mCourse!!.name)
            content.input_theory_hours.editText!!.setText(
                String.format(Locale.getDefault(), "%d", mCourse!!.theoryHours))
            content.input_practical_hours.editText!!.setText(
                String.format(Locale.getDefault(), "%d", mCourse!!.practicalHours))
        }
    }

    private fun setUpTextChangeListeners() {
        input_course_name.editText!!.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}

            override fun afterTextChanged(s: Editable) {
                validateCourseName()
            }
        })

        input_theory_hours.editText!!.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable) {
                validateTheoryHours()
            }
        })

        input_practical_hours.editText!!.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {

            }

            override fun afterTextChanged(s: Editable) {
                validatePracticalHours()
            }
        })
    }

    private fun validateCourseName(): Boolean {
        val name = input_course_name.editText!!.text.toString().trim()
        if (name.isEmpty()) {
            input_course_name.error = getString(R.string.course_edit_name_empty_error)
            return false
        }
        mCourse!!.name = name
        input_course_name.error = null
        input_course_name.isErrorEnabled = false
        return true
    }

    private fun validateTheoryHours(): Boolean {
        val s = input_theory_hours.editText!!.text.toString().trim()
        val i: Int
        try {
            i = Integer.parseInt(s)
        } catch (e: NumberFormatException) {
            input_theory_hours.error = getString(R.string.course_edit_theory_hours_number_error)
            return false
        }
        mCourse!!.theoryHours = i
        input_theory_hours.error = null
        input_theory_hours.isErrorEnabled = false
        return true
    }

    private fun validatePracticalHours(): Boolean {
        val s = input_practical_hours.editText!!.text.toString().trim()
        val i: Int
        try {
            i = Integer.parseInt(s)
        } catch (e: NumberFormatException) {
            input_practical_hours.error = getString(R.string.course_edit_practical_hours_number_error)
            return false
        }
        mCourse!!.practicalHours = i
        input_practical_hours.error = null
        input_practical_hours.isErrorEnabled = false
        return true
    }

    @Suppress("UNUSED_PARAMETER")
    fun submitForm(view: android.view.View) {
        if (!validateCourseName() or !validateTheoryHours() or !validatePracticalHours()) {
            return
        }

        if (mCourse!!.id == 0) {
            viewModel.insert(mCourse!!)
            onBackPressed()
        } else {
            viewModel.update(mCourse!!)
            onBackPressed()
        }

    }

}
