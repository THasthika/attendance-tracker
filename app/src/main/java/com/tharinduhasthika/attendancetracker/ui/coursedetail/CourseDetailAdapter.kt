package com.tharinduhasthika.attendancetracker.ui.coursedetail

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tharinduhasthika.attendancetracker.R
import com.tharinduhasthika.attendancetracker.db.AbsentEntry
import com.tharinduhasthika.attendancetracker.libs.AppViewHolder
import com.tharinduhasthika.attendancetracker.libs.DateHelper
import com.tharinduhasthika.attendancetracker.libs.StringHelper
import kotlinx.android.synthetic.main.item_course_detail.view.*
import java.util.*

class CourseDetailAdapter(private val context: Context, private val listener: AppViewHolder.OnItemClickListener<AbsentEntry>) : RecyclerView.Adapter<CourseDetailAdapter.ViewHolder>() {

    var absentEntries: List<AbsentEntry> = arrayListOf()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CourseDetailAdapter.ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_course_detail, parent, false)
        return ViewHolder(view, context, listener)
    }

    override fun getItemCount(): Int {
        return absentEntries.size
    }

    override fun onBindViewHolder(holder: CourseDetailAdapter.ViewHolder, position: Int) {
        holder.setAbsentEntry(absentEntries[position])
    }

    class ViewHolder(itemView: View, private val context: Context, listener: OnItemClickListener<AbsentEntry>) :
        AppViewHolder<AbsentEntry>(
        itemView,
        listener
    ) {

        init {
            itemView.setOnClickListener(this)
            itemView.setOnLongClickListener(this)
        }

        fun setAbsentEntry(absentEntry: AbsentEntry) {

            item = absentEntry

            val abDate = DateHelper.stringToDate(absentEntry.date)
            itemView.tv_date.text = DateHelper.dateToDisplayDate(abDate!!)

            val h = absentEntry.hours
            val ho = String.format(Locale.getDefault(), "%d", h) + " " + if (h > 1) context.getString(R.string.hours) else context.getString(R.string.hour)
            itemView.tv_hours.text = ho

            itemView.tv_type.text = StringHelper.getAbsentEntryTypeString(absentEntry.type, context)

            if (absentEntry.medicalSubmitted) {
                itemView.tv_medical_status.text = context.getString(R.string.medical_submitted)
            } else {
                itemView.tv_medical_status.text = ""
            }

            if (absentEntry.description.isEmpty()) {
                itemView.tv_description.visibility = View.GONE
                itemView.tv_description.text = ""
            } else {
                itemView.tv_description.visibility = View.VISIBLE
                itemView.tv_description.text = absentEntry.description
            }
        }
    }

}