package com.tharinduhasthika.attendancetracker.ui.absententryedit

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.tharinduhasthika.attendancetracker.db.AbsentEntry
import com.tharinduhasthika.attendancetracker.db.AppRepository
import com.tharinduhasthika.attendancetracker.db.Course
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class AbsentEntryEditViewModel(application: Application) : AndroidViewModel(application) {

    private var parentJob = Job()
    private val coroutineContext: CoroutineContext
        get() = parentJob + Dispatchers.Main
    private val scope = CoroutineScope(coroutineContext)

    private val repository = AppRepository(application)

    fun getAbsentEntryByID(id: Int): LiveData<AbsentEntry> {
        return repository.getAbsentEntryByID(id)
    }

    fun getAbsentEntriesByCourseID(id: Int): LiveData<List<AbsentEntry>> {
        return repository.getAbsentEntriesByCourseID(id)
    }

    fun getCourseByID(id: Int): LiveData<Course> {
        return repository.getCourseByID(id)
    }

    fun update(absentEntry: AbsentEntry) = scope.launch(Dispatchers.IO) {
        repository.update(absentEntry)
    }

    fun insert(absentEntry: AbsentEntry) = scope.launch(Dispatchers.IO) {
        repository.insert(absentEntry)
    }

    fun delete(absentEntry: AbsentEntry) = scope.launch(Dispatchers.IO) {
        repository.delete(absentEntry)
    }

}