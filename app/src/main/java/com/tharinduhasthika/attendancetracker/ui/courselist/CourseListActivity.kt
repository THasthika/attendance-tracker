package com.tharinduhasthika.attendancetracker.ui.courselist

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.tharinduhasthika.attendancetracker.R
import com.tharinduhasthika.attendancetracker.db.AbsentEntry
import com.tharinduhasthika.attendancetracker.db.Course
import com.tharinduhasthika.attendancetracker.libs.AppViewHolder
import com.tharinduhasthika.attendancetracker.libs.AttendanceResult
import com.tharinduhasthika.attendancetracker.ui.coursedetail.CourseDetailActivity
import com.tharinduhasthika.attendancetracker.ui.courseedit.CourseEditActivity
import com.tharinduhasthika.attendancetracker.ui.courseedit.CourseEditViewModel
import kotlinx.android.synthetic.main.activity_course_list.*
import kotlinx.android.synthetic.main.content_course_list.view.*
import java.util.ArrayList
import java.util.HashMap

class CourseListActivity : AppCompatActivity(), AppViewHolder.OnItemClickListener<Course> {

    private lateinit var viewModel: CourseListViewModel
    private var mCourses: List<Course>? = null
    private var mAbsentEntries: List<AbsentEntry>? = null

    private val courseListAdapter = CourseListAdapter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_course_list)

        setSupportActionBar(toolbar)

        viewModel = ViewModelProviders.of(this).get(CourseListViewModel::class.java)

        viewModel.getAllAbsentEntries().observe(this, Observer {
            mAbsentEntries = it
            updateAttendanceResults()
        })

        viewModel.getAllCourses().observe(this, Observer {
            mCourses = it
            updateAttendanceResults()
        })

        content.recyclerview_courses_list.addItemDecoration(DividerItemDecoration(this, DividerItemDecoration.HORIZONTAL))
        content.recyclerview_courses_list.layoutManager = LinearLayoutManager(this)
        content.recyclerview_courses_list.setHasFixedSize(true)
        content.recyclerview_courses_list.adapter = courseListAdapter


        fab.setOnClickListener {
            val intent = Intent(this, CourseEditActivity::class.java)
            startActivity(intent)
        }

        fab.setOnLongClickListener {
            Toast.makeText(applicationContext, R.string.new_course, Toast.LENGTH_SHORT).show()
            true
        }

//        content.recyclerview_courses_list.addOnScrollListener(object : RecyclerView.OnScrollListener() {
//            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
//                if (dy > 0)
//                    fab.hide()
//                else if (dy < 0)
//                    fab.show()
//            }
//        })


    }

    private fun updateAttendanceResults() {
        if (mCourses == null || mAbsentEntries == null) return

        val attendanceResults = ArrayList<AttendanceResult>()

        val courseListHashMap = HashMap<Int, ArrayList<AbsentEntry>>()

        for (absentEntry in mAbsentEntries!!) {
            if (courseListHashMap[absentEntry.course] == null) {
                val absentEntries = ArrayList<AbsentEntry>()
                absentEntries.add(absentEntry)
                courseListHashMap[absentEntry.course] = absentEntries
            } else {
                courseListHashMap[absentEntry.course]!!.add(absentEntry)
            }
        }

        for (course in mCourses!!) {
            var absentEntries = courseListHashMap[course.id]
            if (absentEntries == null) {
                absentEntries = ArrayList()
            }
            attendanceResults.add(AttendanceResult.calculateResult(course, absentEntries))
        }

        courseListAdapter.attendanceResults = attendanceResults
    }

    override fun onItemClick(item: Course) {
        val intent = Intent(this, CourseDetailActivity::class.java)
        intent.putExtra(CourseDetailActivity.DATA_COURSE_ID, item.id)
        startActivity(intent)
    }

    override fun onItemLongClick(item: Course) {
        val intent = Intent(this, CourseEditActivity::class.java)
        intent.putExtra(CourseEditActivity.DATA_COURSE_ID, item.id)
        startActivity(intent)
    }

}
