package com.tharinduhasthika.attendancetracker.ui.coursedetail

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.tharinduhasthika.attendancetracker.db.AbsentEntry
import com.tharinduhasthika.attendancetracker.db.AppRepository
import com.tharinduhasthika.attendancetracker.db.Course
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class CourseDetailViewModel(application: Application) : AndroidViewModel(application) {

    private var parentJob = Job()
    private val coroutineContext: CoroutineContext
        get() = parentJob + Dispatchers.Main
    private val scope = CoroutineScope(coroutineContext)

    private val repository = AppRepository(application)

    private var course: LiveData<Course>? = null
    private var absentEntries: LiveData<List<AbsentEntry>>? = null

    override fun onCleared() {
        super.onCleared()
        parentJob.cancel()
    }

    fun setCourseID(id: Int) {
        course = repository.getCourseByID(id)
        absentEntries = repository.getAbsentEntriesByCourseID(id)
    }

    fun getCourse(): LiveData<Course> {
        return course!!
    }

    fun getAbsentEntries(): LiveData<List<AbsentEntry>> {
        return absentEntries!!
    }

    fun delete(course: Course) = scope.launch(Dispatchers.IO) {
        repository.delete(course)
    }

    fun delete(absentEntry: AbsentEntry) = scope.launch(Dispatchers.IO) {
        repository.delete(absentEntry)
    }

}