package com.tharinduhasthika.attendancetracker.ui.absententryedit

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.content.Context
import android.graphics.Rect
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.MotionEvent
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.google.android.material.snackbar.Snackbar
import com.tharinduhasthika.attendancetracker.R
import com.tharinduhasthika.attendancetracker.db.AbsentEntry
import com.tharinduhasthika.attendancetracker.db.AbsentEntryType
import com.tharinduhasthika.attendancetracker.db.Course
import com.tharinduhasthika.attendancetracker.libs.DateHelper
import kotlinx.android.synthetic.main.activity_absent_entry_edit.*
import kotlinx.android.synthetic.main.content_absent_entry_edit.*
import kotlinx.android.synthetic.main.content_absent_entry_edit.view.*
import java.util.*

class AbsentEntryEditActivity : AppCompatActivity() {

    companion object {
        const val COURSE_ID = "AbsentEntryEditActivity_COURSE_ID"
        const val ABSENT_ENTRY_ID = "AbsentEntryEditActivity_ABSENT_ENTRY_ID"
    }

    private var mAbsentEntry: AbsentEntry? = null
    private var mCourse: Course? = null
    private var mAbsentEntries: List<AbsentEntry>? = null

    private lateinit var viewModel: AbsentEntryEditViewModel

    private val calendar = Calendar.getInstance()

    private var absentEntryDate: Date? = null
    private var absentEntryHours: Int = 0
    private var absentEntryType: AbsentEntryType = AbsentEntryType.THEORY


    private var dateSetListener: DatePickerDialog.OnDateSetListener =
        DatePickerDialog.OnDateSetListener { _, year, month, dayOfMonth ->
            calendar.set(Calendar.YEAR, year)
            calendar.set(Calendar.MONTH, month)
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            absentEntryDate = calendar.time

            updateDateLabel()

            validateDate()
        }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        menuInflater.inflate(R.menu.menu_absent_entry_edit, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onPrepareOptionsMenu(menu: Menu): Boolean {
        if (mAbsentEntry == null) {
            menu.getItem(0).isVisible = false
        }
        return super.onPrepareOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        when (id) {
            R.id.menu_delete -> if (mAbsentEntry != null) {
                AlertDialog.Builder(this)
                    .setTitle(R.string.delete_absent_entry_title)
                    .setMessage(R.string.delete_absent_entry_message)
                    .setIcon(R.drawable.ic_action_alert)
                    .setPositiveButton(android.R.string.yes) { _, _ ->
                        viewModel.delete(mAbsentEntry!!)
                        onBackPressed()
                    }
                    .setNegativeButton(android.R.string.no, null).show()
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_absent_entry_edit)
        setSupportActionBar(toolbar)

        supportActionBar!!.setDisplayHomeAsUpEnabled(true)
        supportActionBar!!.setHomeButtonEnabled(true)

        viewModel = ViewModelProviders.of(this).get(AbsentEntryEditViewModel::class.java)

        val courseId = intent.getIntExtra(COURSE_ID, 0)
        val absentEntryId = intent.getIntExtra(ABSENT_ENTRY_ID, 0)

        if (courseId == 0) {
            Toast.makeText(applicationContext, "ERROR OCCURED!", Toast.LENGTH_SHORT).show()
            finish()
            return
        }

        viewModel.getCourseByID(courseId).observe(this, Observer {
            mCourse = it
            updateForm()
        })

        viewModel.getAbsentEntriesByCourseID(courseId).observe(this, Observer {
            mAbsentEntries = it
            updateForm()
        })

        if (absentEntryId == -1) {
            content.radio_theory.isChecked = true
            absentEntryType = AbsentEntryType.THEORY
            supportActionBar!!.setTitle(R.string.new_absent_entry)
        } else {
            supportActionBar!!.setTitle(R.string.edit_absent_entry)
            viewModel.getAbsentEntryByID(absentEntryId).observe(this, Observer {
                mAbsentEntry = it
                updateForm()
            })
        }

        content.radio_group_types.setOnCheckedChangeListener { _, checkedId ->
            absentEntryType = if (checkedId == R.id.radio_theory) {
                AbsentEntryType.THEORY
            } else {
                AbsentEntryType.PRACTICAL
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    override fun dispatchTouchEvent(ev: MotionEvent): Boolean {
        if (ev.action == MotionEvent.ACTION_DOWN) {
            val v = currentFocus
            if (v is EditText) {
                val outRect = Rect()
                v.getGlobalVisibleRect(outRect)
                if (!outRect.contains(ev.rawX.toInt(), ev.rawY.toInt())) {
                    v.clearFocus()
                    val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                    imm.hideSoftInputFromWindow(v.getWindowToken(), 0)
                }
            }
        }
        return super.dispatchTouchEvent(ev)
    }

    private fun updateForm() {
        if (mAbsentEntry == null)
            return

        absentEntryDate = DateHelper.stringToDate(mAbsentEntry!!.date)
        calendar.time = absentEntryDate
        updateDateLabel()

        absentEntryHours = mAbsentEntry!!.hours
        content.input_absent_entry_hours.editText?.setText(String.format(Locale.getDefault(), "%d", absentEntryHours))

        content.input_absent_entry_description.editText?.setText(mAbsentEntry!!.description)

        if (mAbsentEntry!!.type === AbsentEntryType.THEORY) {
            content.radio_theory.isChecked = true
            absentEntryType = AbsentEntryType.THEORY
        } else {
            content.radio_practical.isChecked = true
            absentEntryType = AbsentEntryType.PRACTICAL
        }

        content.checkbox_medical.isChecked = mAbsentEntry!!.medicalSubmitted
    }

    private fun updateDateLabel() {
        inputAbsentEntryDate.text = DateHelper.dateToDisplayDate(absentEntryDate!!)
    }

    private fun validateDate(): Boolean {
        if (absentEntryDate == null) {
            content.inputAbsentEntryDate.error = getString(R.string.absent_entry_edit_date_set_error)
            Snackbar.make(rootLayout, R.string.absent_entry_edit_date_set_error, Snackbar.LENGTH_SHORT).show()
            return false
        }
        content.inputAbsentEntryDate.error = null
        return true
    }

    private fun validateHours(): Boolean {
        val s = content.input_absent_entry_hours.editText?.text.toString().trim()
        val i: Int
        try {
            i = Integer.parseInt(s)
        } catch (e: NumberFormatException) {
            content.input_absent_entry_hours.error = getString(R.string.absent_entry_edit_hours_number_error)
            return false
        }

        absentEntryHours = i
        content.input_absent_entry_hours.error = null
        content.input_absent_entry_hours.isErrorEnabled = false
        return true
    }

    @Suppress("UNUSED_PARAMETER")
    fun submitForm(view: View) {
        if (!validateDate() or !validateHours()) {
            return
        }

        if (mCourse == null)
            return

        val description = content.input_absent_entry_description.editText?.text.toString()
        val medical = content.checkbox_medical.isChecked

        if (mAbsentEntry == null) {
            mAbsentEntry = AbsentEntry(
                0,
                DateHelper.dateToString(absentEntryDate!!),
                absentEntryType,
                absentEntryHours,
                medical,
                description,
                mCourse!!.id
            )
        } else {
            mAbsentEntry!!.date = DateHelper.dateToString(absentEntryDate!!)
            mAbsentEntry!!.hours = absentEntryHours
            mAbsentEntry!!.type = absentEntryType
            mAbsentEntry!!.description = description
            mAbsentEntry!!.medicalSubmitted = medical
        }

        if (!canSave()) {
            content.input_absent_entry_hours.error = "Exceeds allowed maximum lecture hours"
            return
        } else {
            content.input_absent_entry_hours.error = null
            content.input_absent_entry_hours.isErrorEnabled = false
        }

        if (mAbsentEntry!!.id == 0) {
            viewModel.insert(mAbsentEntry!!)
            onBackPressed()
        } else {
            viewModel.update(mAbsentEntry!!)
            onBackPressed()
        }
    }

    @Suppress("UNUSED_PARAMETER")
    fun showDatePicker(view: View) {
        val dialog = DatePickerDialog(
            this@AbsentEntryEditActivity, dateSetListener,
            calendar.get(Calendar.YEAR),
            calendar.get(Calendar.MONTH),
            calendar.get(Calendar.DAY_OF_MONTH)
        )
        dialog.show()
    }

    private fun canSave(): Boolean {
        var abh = 0
        for (absentEntry in mAbsentEntries!!) {
            if (absentEntry.type === mAbsentEntry!!.type && absentEntry.id != mAbsentEntry!!.id) {
                abh += absentEntry.hours
            }
        }
        val maxHours = if (mAbsentEntry!!.type === AbsentEntryType.THEORY) mCourse!!.theoryHours else mCourse!!.practicalHours
        return maxHours - abh - mAbsentEntry!!.hours >= 0
    }

}
