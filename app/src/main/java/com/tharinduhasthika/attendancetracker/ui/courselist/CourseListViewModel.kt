package com.tharinduhasthika.attendancetracker.ui.courselist

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import com.tharinduhasthika.attendancetracker.db.AbsentEntry
import com.tharinduhasthika.attendancetracker.db.AppRepository
import com.tharinduhasthika.attendancetracker.db.Course
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlin.coroutines.CoroutineContext

class CourseListViewModel(application: Application) : AndroidViewModel(application) {

    private var parentJob = Job()
    private val coroutineContext: CoroutineContext
        get() = parentJob + Dispatchers.Main
    private val scope = CoroutineScope(coroutineContext)

    private val repository = AppRepository(application)

    fun getAllCourses(): LiveData<List<Course>> {
        return repository.getAllCourses()
    }

    fun getAllAbsentEntries(): LiveData<List<AbsentEntry>> {
        return repository.getAllAbsentEntries()
    }


}