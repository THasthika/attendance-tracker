package com.tharinduhasthika.attendancetracker.db

import androidx.room.*

@Entity(tableName = "absent_entries")
data class AbsentEntry (
    @PrimaryKey(autoGenerate = true) var id: Int,
    var date: String,
    var type: AbsentEntryType,
    var hours: Int,
    @ColumnInfo(name = "medical_submitted") var medicalSubmitted: Boolean,
    var description: String,
    @ColumnInfo(index = true) var course: Int
)

