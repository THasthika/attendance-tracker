package com.tharinduhasthika.attendancetracker.db

import android.app.Application
import androidx.annotation.WorkerThread
import androidx.lifecycle.LiveData

class AppRepository(private val application: Application) {

    private val appDatabase = AppDatabase.invoke(application)
    private val courseDao : CourseDao
    private val absentEntryDao : AbsentEntryDao

    init {
        courseDao = appDatabase.courseDao()
        absentEntryDao = appDatabase.absentEntryDao()
    }

    // course functions

    @WorkerThread
    suspend fun insert(course: Course) {
        courseDao.insert(course)
    }

    @WorkerThread
    suspend fun update(course: Course) {
        courseDao.update(course)
    }

    @WorkerThread
    suspend fun delete(course: Course) {
        courseDao.delete(course)
    }

    fun getAllCourses() : LiveData<List<Course>> {
        return courseDao.getAllCourses()
    }

    fun getCourseByID(id: Int) : LiveData<Course> {
        return courseDao.getByID(id)
    }

    // absent entry functions

    @WorkerThread
    suspend fun insert(absentEntry: AbsentEntry) {
        absentEntryDao.insert(absentEntry)
    }

    @WorkerThread
    suspend fun update(absentEntry: AbsentEntry) {
        absentEntryDao.update(absentEntry)
    }

    @WorkerThread
    suspend fun delete(absentEntry: AbsentEntry) {
        absentEntryDao.delete(absentEntry)
    }

    fun getAllAbsentEntries() : LiveData<List<AbsentEntry>> {
        return absentEntryDao.getAllAbsentEntries()
    }

    fun getAbsentEntryByID(id: Int) : LiveData<AbsentEntry> {
        return absentEntryDao.getById(id)
    }

    fun getAbsentEntriesByCourseID(id: Int) : LiveData<List<AbsentEntry>> {
        return absentEntryDao.getByCourseID(id)
    }


}