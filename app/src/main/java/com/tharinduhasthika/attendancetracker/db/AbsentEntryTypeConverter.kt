package com.tharinduhasthika.attendancetracker.db

import androidx.room.TypeConverter

class AbsentEntryTypeConverter {
    companion object {
        @TypeConverter
        @JvmStatic
        fun fromInstant(absentEntryType: AbsentEntryType): Int {
            return absentEntryType.ordinal
        }

        @TypeConverter
        @JvmStatic
        fun toInstant(absentEntryTypeInt: Int): AbsentEntryType? {
            if (AbsentEntryType.THEORY.ordinal == absentEntryTypeInt) {
                return AbsentEntryType.THEORY
            } else if (AbsentEntryType.PRACTICAL.ordinal == absentEntryTypeInt) {
                return AbsentEntryType.PRACTICAL
            }
            return null
        }
    }
}