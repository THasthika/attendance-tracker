package com.tharinduhasthika.attendancetracker.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase

@Database(entities = [Course::class, AbsentEntry::class], version = 2)
@TypeConverters(AbsentEntryTypeConverter::class)
abstract class AppDatabase : RoomDatabase() {

    abstract fun courseDao() : CourseDao
    abstract fun absentEntryDao() : AbsentEntryDao

    companion object {
        @Volatile private var instance: AppDatabase? = null
        private val LOCK = Any()

        operator fun invoke(context: Context)= instance ?: synchronized(LOCK){
            instance ?: buildDatabase(context).also { instance = it}
        }

        private fun buildDatabase(context: Context) = Room.databaseBuilder(context, AppDatabase::class.java, "attendance_database")
            .addMigrations(MIGRATION_1_2)
            .fallbackToDestructiveMigration()
            .build()

        private val MIGRATION_1_2: Migration = object : Migration(1, 2) {
            override fun migrate(database: SupportSQLiteDatabase) {

                database.execSQL("ALTER TABLE absent_entries " + "ADD COLUMN medical_submitted INTEGER DEFAULT 0 NOT NULL;")

                database.execSQL("ALTER TABLE absent_entries " + "ADD COLUMN description TEXT DEFAULT '';")

            }
        }
    }

}