package com.tharinduhasthika.attendancetracker.db

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface AbsentEntryDao {

    @Insert
    fun insert(absentEntry: AbsentEntry)

    @Update
    fun update(absentEntry: AbsentEntry)

    @Delete
    fun delete(absentEntry: AbsentEntry)

    @Query("SELECT * FROM absent_entries WHERE id = :id")
    fun getById(id: Int): LiveData<AbsentEntry>

    @Query("SELECT * FROM absent_entries ORDER BY date DESC")
    fun getAllAbsentEntries(): LiveData<List<AbsentEntry>>

    @Query("SELECT * FROM absent_entries WHERE course = :courseId ORDER BY date DESC")
    fun getByCourseID(courseId: Int): LiveData<List<AbsentEntry>>

}