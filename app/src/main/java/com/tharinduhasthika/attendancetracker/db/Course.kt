package com.tharinduhasthika.attendancetracker.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "courses")
data class Course (
    @PrimaryKey(autoGenerate = true) var id: Int,
    var name: String,
    @ColumnInfo(name = "theory_hours") var theoryHours: Int,
    @ColumnInfo(name = "practical_hours") var practicalHours: Int
)