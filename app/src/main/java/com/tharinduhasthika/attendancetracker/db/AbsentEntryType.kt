package com.tharinduhasthika.attendancetracker.db

enum class AbsentEntryType {
    THEORY, PRACTICAL
}