package com.tharinduhasthika.attendancetracker.db

import androidx.lifecycle.LiveData
import androidx.room.*

@Dao
interface CourseDao {
    @Insert
    fun insert(course: Course)

    @Update
    fun update(course: Course)

    @Delete
    fun delete(course: Course)

    @Query("SELECT * FROM courses WHERE id = :id")
    fun getByID(id: Int): LiveData<Course>

    @Query("SELECT * FROM courses ORDER BY name ASC")
    abstract fun getAllCourses(): LiveData<List<Course>>
}